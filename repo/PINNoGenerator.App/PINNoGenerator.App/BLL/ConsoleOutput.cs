﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PINNoGenerator.App.Interface;
using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Entities;
using System.Text.RegularExpressions;
namespace PINNoGenerator.App.BLL
{
    public class ConsoleOutput : IOuput
    {
        private readonly ISequenceRepository _sequenceRpository;
        private readonly IInvalidSeqenceRepository _invalidSequenceRpository;

        private const int MIN = 0;
        private const int MAX = 9999;
        public ConsoleOutput(ISequenceRepository sequenceRepository,
            IInvalidSeqenceRepository invalidSequenceRepository)
        {
            _sequenceRpository = sequenceRepository;
            _invalidSequenceRpository = invalidSequenceRepository;
        }

        public void Menu()
        {
            Console.WriteLine("Please enter your options...");
            Console.WriteLine("Generating PIN Number(s)-----------------------1");
            Console.WriteLine("Show currect Valid PIN in Database-------------2");
            Console.WriteLine("Insert an Invalid PIN Number to avoid----------3");
            Console.WriteLine("Show currect InValid PIN-----------------------4");
            Console.WriteLine("Exit-------------------------------------------5");

            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    InsertValidSequence();
                    break;
                case "2":
                    PrintAllValidSequence();
                    break;
                case "4":
                    PrintAllInvalidSequence();
                    break;
                case "3":
                    InsertInValidSequence();
                    break;
                case "5":
                    break;
                default:
                    Menu();
                    break;
            }
        }
        //Get all Invalid Sequence and output to console
        private void PrintAllInvalidSequence()
        {
            Console.WriteLine("------------------Invalid Sequences----------------");
            List<InValidSequence> invaild = _invalidSequenceRpository.GetAllInvalidSequence();

            foreach (InValidSequence inv in invaild)
            {
                Console.Write(inv.Sequence + "\t");
            }
            Console.WriteLine("\n---------------------------------------------------\n");
            Menu();
        }
        private void PrintAllValidSequence()
        {
            Console.WriteLine("------------------Valid Sequences----------------");
            List<Sequence> valid = _sequenceRpository.GetAllSequence();

            foreach (Sequence inv in valid)
            {
                Console.Write(inv.Sequence + "\t");
            }
            Console.WriteLine("\n---------------------------------------------------\n");
            Menu();
        }
        //read number from console and generate number(s) to the valid table
        private void InsertValidSequence()
        {
            try
            {
                uint PinsToGenerate;
                bool inserted;
                Console.WriteLine("\n---------------------------------------------------\n");
                Console.WriteLine("Enter the no of PINs to Generate");
                PinsToGenerate = Convert.ToUInt32(Console.ReadLine());
                for (int i = 0; i < PinsToGenerate; i++)
                {
                    do
                    {
                        inserted = false;
                        var numberTogenerate = GenerateNumber();
                        // check that the number of sequence has exhaused all the valid sequence
                        if ((MAX - MIN) <= _sequenceRpository.TotalNoValidSeq() - _invalidSequenceRpository.TotalNoImValidSeq())
                        {
                            //insert to the table
                            inserted = _sequenceRpository.Save(new Sequence { Sequence = numberTogenerate });
                        }
                        //if not all valid sequence is exhaused then check for duplication
                        else if (!_sequenceRpository.Exists(numberTogenerate))
                        {
                            //insert to the table
                            inserted = _sequenceRpository.Save(new Sequence { Sequence = numberTogenerate });
                        }

                    } while (!inserted);
                }
                Menu();
            }
            catch (Exception e)
            {
                Console.WriteLine("\n---------------------------------------------------\n");
                Console.WriteLine("Please check Mongo Server is running OR Invalid Entry");
                Console.WriteLine("\n---------------------------------------------------\n");
                InsertValidSequence();
            }

        }

        // Generate Number and validate using regular expression
        // insert to Invalid sequence table if invalid
        // return number as string if valid
        private string GenerateNumber()
        {
            Random number = new Random();
            string ranNumber;
            Match noSequentialMatch, noDigitRepeatsMatch;
            do
            {
                ranNumber = number.Next(MIN, MAX).ToString();
                ranNumber = ranNumber.PadLeft(4, '0');
                //regular expression to check for invalid pin
                var noSequential = @"^(?:0?123(45?)?|1?23456?|2?34567?|3?45678?|4?56789?|(5?6)?7890?|
                                        (0?1)?2345678?(90$)?|1?23456789?|2?345678(90?)?)$";
                var noDigitRepeats = @"^([0-9])\1*$";

                noSequentialMatch = Regex.Match(ranNumber, noSequential, RegexOptions.None);
                noDigitRepeatsMatch = Regex.Match(ranNumber, noDigitRepeats, RegexOptions.None);

                //if the sequence is not valid then try to add to the invalid sequence table
                if ((noSequentialMatch.Success || noDigitRepeatsMatch.Success) && !_invalidSequenceRpository.Exists(ranNumber))
                {
                    _invalidSequenceRpository.Save(new InValidSequence { Sequence = ranNumber });
                }

            } while (noSequentialMatch.Success && noDigitRepeatsMatch.Success);

            return ranNumber;
        }
        // if the users wishes to add more invalid seqences (i.e. 0001)
        // this function will add any sequence to invalid sequence table provided that sequence doesn't already exist in the table
        private void InsertInValidSequence()
        {
            string invalidSequence;
            bool inserted = false;
            try
            {
                Console.WriteLine("\n---------------------------------------------------\n");
                Console.WriteLine("Please Enter an Invalid Sequence:");
                invalidSequence = Console.ReadLine();
                var invalidSeqToInsert = invalidSequence.PadLeft(4, '0');
                inserted = InsertInvalidSeq(invalidSeqToInsert);
                Menu();

            }
            catch (Exception e)
            {
                Console.WriteLine("\n---------------------------------------------------\n");
                Console.WriteLine("Please check Mongo Server is running, Invalid Format");
                Console.WriteLine("\n---------------------------------------------------\n");
            }
        }
        private bool InsertInvalidSeq(string invalidSeqToInsert)
        {
            if (!_invalidSequenceRpository.Exists(invalidSeqToInsert))
            {
                return _invalidSequenceRpository.Save(new InValidSequence { Sequence = invalidSeqToInsert });
            }
            else
            {
                Console.WriteLine("\n---------------------------------------------------\n");
                Console.WriteLine("Sequence already exists");
                Console.WriteLine("\n---------------------------------------------------\n");
            }
            return false;
        }
    }
}
