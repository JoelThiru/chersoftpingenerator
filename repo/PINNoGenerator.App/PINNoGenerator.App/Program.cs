﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Ninject;
using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Entities;
using System.Text.RegularExpressions;
using PINNoGenerator.App.Interface;
using PINNoGenerator.App.BLL;

namespace PINNoGenerator.App
{
    public class Program
    {

        public static void Main(string[] args)
        {
            //Dependency Injection
            IKernel kernel = new StandardKernel();
            kernel.Load(System.Reflection.Assembly.GetExecutingAssembly());
            ISequenceRepository _sequenceRpository = kernel.Get<ISequenceRepository>();
            IInvalidSeqenceRepository _invalidSequenceRpository = kernel.Get<IInvalidSeqenceRepository>();


            //User Interface
            IOuput console = new ConsoleOutput(_sequenceRpository, _invalidSequenceRpository);
            console.Menu();
        }

    }

}
