﻿using System;
using Ninject;
using Ninject.Modules;
using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Repository;
using PINNoGenerator.App.Interface;
using PINNoGenerator.App.BLL;

namespace PINNoGenerator.App
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            //registering to the Kernal
            Bind<ISequenceRepository>().To<SequenceRepository>();
            Bind<IInvalidSeqenceRepository>().To<InvalidSequenceRepository>();
        }
    }
}
