﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PINNoGenerator.Data.Entities;
using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Repository;
using System.Text.RegularExpressions;
namespace PINNoGenerator.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void AddValidSequence()
        {
            Sequence seq = new Sequence { Sequence = "1235" };
            ISequenceRepository repo = new SequenceRepository();
            repo.Save(seq);
            Assert.IsTrue(repo.Exists(seq), "Sequence Exist");
        }

        [TestMethod]
        public void CheckSequence()
        {
            Sequence seq = new Sequence { Sequence = "7894" };
            ISequenceRepository repo = new SequenceRepository();
            Assert.IsFalse(repo.Exists(seq), "Sequence doesnt exist");
        }

        [TestMethod]
        public void CheckInvalidSequence()
        {
            ISequenceRepository repo = new SequenceRepository();
            Assert.IsFalse(repo.Exists("1234"), "Invalid Sequence doesnt exist");
            Assert.IsFalse(repo.Exists("1111"), "Invalid Sequence doesnt exist");
            Assert.IsFalse(repo.Exists("8888"), "Invalid Sequence doesnt exist");
        }
    }
}
