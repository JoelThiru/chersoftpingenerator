﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using PINNoGenerator.Data.Interface;
using System;

namespace PINNoGenerator.Data.Entities
{
    public class MongoEntity : IMongoEntity
    {
        [BsonId(IdGenerator = typeof(CombGuidGenerator))]
        public Guid Id { get; set; }
        public string Sequence { get; set; }
    }
}
