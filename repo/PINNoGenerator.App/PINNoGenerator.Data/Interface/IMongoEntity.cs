﻿using System;
using MongoDB.Bson;

namespace PINNoGenerator.Data.Interface
{
    public interface IMongoEntity
    {
        Guid Id { get; set; }

        string Sequence { get; set; }
    }
}
