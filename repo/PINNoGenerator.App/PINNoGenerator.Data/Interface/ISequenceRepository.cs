﻿using PINNoGenerator.Data.Entities;
using System;
using System.Collections.Generic;

namespace PINNoGenerator.Data.Interface
{
    public interface ISequenceRepository
    {
        bool Save(Sequence sequence);
        List<Sequence> GetAllSequence();
        bool Exists(string seq);
        bool Exists(Sequence seq);
        long TotalNoValidSeq();
    }
}
