﻿using System;
using System.Collections.Generic;
using PINNoGenerator.Data.Entities;
namespace PINNoGenerator.Data.Interface
{
    public interface IInvalidSeqenceRepository
    {
        bool Save(InValidSequence invSequence);
        List<InValidSequence> GetAllInvalidSequence();
        bool Exists(string seq);
        long TotalNoImValidSeq();
    }
}
