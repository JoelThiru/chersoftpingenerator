﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PINNoGenerator.Data.Interface
{
    public interface IRepository<T> where T :IMongoEntity
    {
        bool Create(T entity);
        bool Delete(string id);
        T FindById(string id);
        List<T> GetAll();
        bool Update(T entity);
        bool Exist(string seq);
    }
}
