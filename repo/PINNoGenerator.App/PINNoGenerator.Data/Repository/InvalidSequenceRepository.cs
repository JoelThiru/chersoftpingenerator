﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Entities;

namespace PINNoGenerator.Data.Repository
{
    public class InvalidSequenceRepository :  Repository<InValidSequence>, IInvalidSeqenceRepository
    {
       
        public bool Save(InValidSequence invSequence, long TotalNo)
        {
            string id = invSequence.Id.ToString();
            var entity = FindById(id);

            if (entity == null)
            {
                return Create(invSequence);
            }
            return false;
        }

        public List<InValidSequence> GetAllInvalidSequence()
        {
            return GetAll().ToList();
        }

        public override bool Update(InValidSequence entity)
        {
            throw new NotImplementedException();
        }
        public bool Exists(string seq)
        {
            return Exist(seq);
        }

        public bool Save(InValidSequence invSequence)
        {
            if (!Exist(invSequence.Sequence))
            {
                return Create(invSequence);
            }
            return false;
        }

        public long TotalNoImValidSeq()
        {
            return Count();
        }
    }
}
