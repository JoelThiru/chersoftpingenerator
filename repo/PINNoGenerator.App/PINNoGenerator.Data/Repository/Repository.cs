﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PINNoGenerator.Data.Interface;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;

namespace PINNoGenerator.Data.Repository
{
    public abstract class Repository<T> : IRepository<T>
        where T : IMongoEntity
    {
        protected readonly MonogoConnectionHandler<T> _mongoConnectionHandler;

        protected Repository()
        {
            _mongoConnectionHandler = new MonogoConnectionHandler<T>();
        }
        public virtual bool Create(T entity)
        {
            var result = _mongoConnectionHandler.MongoCollection.Save(
                entity,
                new MongoInsertOptions
                {
                    WriteConcern = WriteConcern.Acknowledged
                });

            return result.Ok;
        }

        public virtual bool Delete(string id)
        {
            var entityQuery = Query<T>.EQ(e => e.Id, new Guid(id));
            var result = _mongoConnectionHandler.MongoCollection
                .Remove(entityQuery);
            return result.Ok;
        }

        public virtual T FindById(string id)
        {
            var entityQuery = Query<T>.EQ(e => e.Id, new Guid(id));
            return _mongoConnectionHandler.MongoCollection.FindOneAs<T>(entityQuery);
        }

        public abstract bool Update(T entity);
        public virtual List<T> GetAll()
        {
            return _mongoConnectionHandler.MongoCollection.FindAllAs<T>().ToList();
        }

        public virtual long Count()
        {
            return _mongoConnectionHandler.MongoCollection.Count();
        }

        public virtual bool Exist(string seq)
        {
            var entityQuery = Query<T>.EQ(e => e.Sequence, seq);
            return _mongoConnectionHandler.MongoCollection.FindOneAs<T>(entityQuery) == null ? false : true;
        }
        public virtual bool Exist(T seq)
        {
            var entityQuery = Query<T>.EQ(e => e.Id, seq.Id);
            return _mongoConnectionHandler.MongoCollection.FindOneAs<T>(entityQuery) == null ? false : true;
        }
    }
}
