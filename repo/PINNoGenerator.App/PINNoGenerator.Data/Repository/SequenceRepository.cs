﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PINNoGenerator.Data.Interface;
using PINNoGenerator.Data.Entities;


namespace PINNoGenerator.Data.Repository
{
    public class SequenceRepository : Repository<Sequence>, ISequenceRepository
    {
        public bool Save(Sequence sequence)
        {
            return Create(sequence);
        }

        public List<Sequence> GetAllSequence()
        {
            return GetAll().ToList();
        }

        public bool Exists(string seq)
        {
            return Exist(seq);
        }
        public bool Exists(Sequence seq)
        {
            return Exist(seq);
        }

        public override bool Update(Sequence entity)
        {
            throw new NotImplementedException();
        }

        public long TotalNoValidSeq()
        {
            return Count(); 
        }
    }
}
